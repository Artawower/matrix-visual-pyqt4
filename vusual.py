# -*- coding: UTF-8 -*-

import PyQt4, sys
from PyQt4 import QtGui, QtCore
import numpy as np


class AppWind(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.setGeometry(100, 100, 750, 550)
        self.setWindowTitle('Matrix')


        edit = QtGui.QTextEdit('1', self)
        edit.setFixedWidth(120)
        edit.setFixedHeight(25)

        edit2 = QtGui.QTextEdit('1', self)
        edit2.setFixedWidth(120)
        edit2.setFixedHeight(25)

        button_plus = QtGui.QPushButton('+', self)
        button_minus = QtGui.QPushButton('-', self)
        button_mult = QtGui.QPushButton('*', self)
        button_trans = QtGui.QPushButton('T', self)
        button_triangle = QtGui.QPushButton('tr', self)
        button_comparasion = QtGui.QPushButton(u'сравнить', self)

        #
        # button_plus.setGeometry(240, 16, 25, 25)
        # button_minus.setGeometry(280, 16, 25, 25)
        # button_mult.setGeometry(320, 16, 25, 25)

        grid = QtGui.QGridLayout()
        grid.setSpacing(10)
        grid.addWidget(edit, 0, 0)
        grid.addWidget(edit2, 0, 1)
        grid.addWidget(button_plus, 0, 2)
        grid.addWidget(button_minus, 0, 3)
        grid.addWidget(button_mult, 0, 4)
        grid.addWidget(button_trans, 0, 5)
        grid.addWidget(button_triangle, 0, 6)
        grid.addWidget(button_comparasion, 0, 7)

        table = QtGui.QTableWidget(parent)
        matrix_2 = QtGui.QTableWidget(parent)

        table.setColumnCount(0)
        table.setRowCount(0)
        grid.addWidget(table, 1, 0, 5, 5)

        matrix_2.setColumnCount(0)
        matrix_2.setRowCount(0)
        grid.addWidget(matrix_2, 1, 5, 5, 10)

        decision = QtGui.QTableWidget(parent)
        decision.setColumnCount(0)
        decision.setRowCount(0)
        grid.addWidget(decision, 6, 0, 10, 15)

        # table.setGeometry(120, 140, 130, 180)

        # print (table.getContentsMargins())


        col_row = 0;
        matrix1 = []
        matrix2 = []
        matrix_res = []
        tmp_matrix = []


        def set_table_size():
            # table = QtGui.QTableWidget(parent)

            if ((int(edit.toPlainText()) <= 10) and (int(edit2.toPlainText()) <= 10)):
                table.setColumnCount(int(edit.toPlainText()))
                table.setRowCount(int(edit2.toPlainText()))

                matrix_2.setColumnCount(int(edit.toPlainText()))
                matrix_2.setRowCount(int(edit2.toPlainText()))

                decision.setColumnCount(int(edit.toPlainText()))
                decision.setRowCount(int(edit2.toPlainText()))

                for i in range(int(edit.toPlainText())):
                    table.horizontalHeader().resizeSection(i, 40)
                    matrix_2.horizontalHeader().resizeSection(i, 40)
                    decision.horizontalHeader().resizeSection(i, 40)


            else:
                QtGui.QMessageBox.about(self, u"Warning", u'Данная программа может считать лишь матрицы 10х10, для расширения функционала необходимо поплнить баланс разработчика')



        #тут назначем действие к событиям
        self.connect(edit, QtCore.SIGNAL("textChanged()"), lambda : set_table_size())
        self.connect(edit2, QtCore.SIGNAL("textChanged()"), lambda: set_table_size())

        self.connect(button_plus, QtCore.SIGNAL("clicked()"), lambda: result_matrix(matrix_summ(select_matrix1(matrix1, table),
                                                                                                select_matrix1(matrix2, matrix_2))))

        self.connect(button_minus, QtCore.SIGNAL("clicked()"), lambda : result_matrix(matrix_minus(select_matrix1(matrix1, table),
                                                                                                   select_matrix1(matrix2, matrix_2))))

        self.connect(button_trans, QtCore.SIGNAL("clicked()"), lambda  : matrix_trans(select_matrix1(matrix1, table)))

        self.connect(button_mult, QtCore.SIGNAL("clicked()"), lambda : result_matrix(matrix_mult(select_matrix1(matrix1, table),
                                                                                                 select_matrix1(matrix2, matrix_2))))

        self.connect(button_triangle, QtCore.SIGNAL("clicked()"), lambda : result_matrix(triangle_mattix(select_matrix1(matrix1, table))))

        self.connect(button_comparasion, QtCore.SIGNAL("clicked()"), lambda : comarasion(select_matrix1(matrix1, table),
                                                                                         select_matrix1(matrix2, matrix_2)))

        # self.connect(button_plus, QtCore.SIGNAL("clicked()"), lambda : select_matrix1(matrix1, table))
        # self.connect(button_minus, QtCore.SIGNAL("clicked()"), lambda : QtGui.QMessageBox.about(self, "ep", str(col_row)))


        table.setItem(2, 2, QtGui.QTableWidgetItem('12'))
        self.setLayout(grid)

        a = [2.0, 1.0, 1.0], [1.0, -1.0, 0], [3.0, -1.0, 2.0]


        print a[1]



        #очищаем от мусора, подгружаем матрицу
        def select_matrix1(matrix, table):
            del matrix[:]
            try:
                for i in range(int(edit2.toPlainText())):
                    matrix.append([])
                    for j in range(int(edit.toPlainText())):
                        matrix[i].append(float(table.item(i, j).text()))
            except Exception:
                QtGui.QMessageBox.about(self, "erroe", u"Заполните цифрами все ячейки таблицы")
            return matrix


        # сложение
        def matrix_summ(m1, m2):
            del tmp_matrix[:]
            for i in range(int(edit2.toPlainText())):
                tmp_matrix.append([])
                for j in range(int(edit.toPlainText())):
                    tmp_matrix[i].append(int(m1[i][j]) + int(m2[i][j]))

            print tmp_matrix
            return tmp_matrix

        def matrix_minus(m1, m2):
            del tmp_matrix[:]
            for i in range(int(edit2.toPlainText())):
                tmp_matrix.append([])
                for j in range(int(edit.toPlainText())):
                    tmp_matrix[i].append(int(m1[i][j]) - int(m2[i][j]))
            return tmp_matrix

        def matrix_trans(m):
            del tmp_matrix[:]
            decision.setColumnCount(int(edit2.toPlainText()))
            decision.setRowCount(int(edit.toPlainText()))


            for i in range(int(edit.toPlainText())):
                tmp_matrix.append([])
                for j in range(int(edit2.toPlainText())):
                    tmp_matrix[i].append(int(m[j][i]))

            for i in range(int(edit.toPlainText())):
                for j in range(int(edit2.toPlainText())):
                    decision.setItem(i, j, QtGui.QTableWidgetItem(str(tmp_matrix[i][j])))
            return tmp_matrix

        def matrix_mult(m1, m2):
            del tmp_matrix[:]
            summ = 0
            for k in range (int(edit2.toPlainText())):
                tmp_matrix.append([])
                for i in range(int(edit.toPlainText())):
                    for j in range(int(edit.toPlainText())):

                        summ += m1[i][j] * m2[j][i]

                    tmp_matrix[k].append(summ)
                    summ = 0
            return tmp_matrix


        # вывод резульата любых операций в 3 столбец
        def result_matrix(m):
            print m
            for i in range(int(edit2.toPlainText())):
                for j in range(int(edit.toPlainText())):
                    decision.setItem(i, j, QtGui.QTableWidgetItem(str(m[i][j])))


        #приведение к треугольному виду
        def triangle_mattix(m):
            if  (int(edit.toPlainText())) == int(edit2.toPlainText()):
                mat = np.array(m)
                mat[0] = (mat[0]) / mat[0][0]

                k = 0

                while k < mat.shape[1]:
                    for i in range(k + 1, mat.shape[0]):
                        mat[i] = mat[i] / mat[i][k]
                        mat[i] = mat[i] - mat[k]
                        mat[i] = mat[i] / mat[i][k + 1]
                    k += 1

                print mat
                return mat
            else:
                QtGui.QMessageBox.about(self, "error", u'матрица должна быть квадратной')

        #сравнение
        def comarasion(m1, m2):
            comp = False
            for i in range(int(edit2.toPlainText())):
                for j in range(int(edit.toPlainText())):
                    if  m1[i][j] != m2[i][j]:
                        comp = True
            if comp == False:
                QtGui.QMessageBox.about(self, u'сравнение', u'матрицы идентичны')
            else:
                QtGui.QMessageBox.about(self, u'сравнение', u'матрицы не идентичны')

# 11
app = QtGui.QApplication(sys.argv)
rn = AppWind()
rn.show()
sys.exit(app.exec_())









