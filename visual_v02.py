# -*- coding: UTF-8 -*-

import PyQt4, sys
from PyQt4 import QtGui, QtCore
import numpy as np, random


class AppWind(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.setGeometry(100, 100, 750, 550)
        self.setWindowTitle('Matrix')


        self.edit = QtGui.QTextEdit('1', self)
        self.edit.setFixedWidth(70)
        self.edit.setFixedHeight(25)

        self.edit2 = QtGui.QTextEdit('1', self)
        self.edit2.setFixedWidth(70)
        self.edit2.setFixedHeight(25)

        self.edit3 = QtGui.QTextEdit('1', self)
        self.edit3.setFixedWidth(70)
        self.edit3.setFixedHeight(25)

        self.edit4 = QtGui.QTextEdit('1', self)
        self.edit4.setFixedWidth(70)
        self.edit4.setFixedHeight(25)

        self.button_plus = QtGui.QPushButton('+', self)
        self.button_plus.setFixedWidth(70)
        self.button_minus = QtGui.QPushButton('-', self)
        self.button_minus.setFixedWidth(70)
        self.button_mult = QtGui.QPushButton('*', self)
        self.button_mult.setFixedWidth(70)
        self.button_trans = QtGui.QPushButton('T', self)
        self.button_trans.setFixedWidth(70)
        self.button_triangle = QtGui.QPushButton('tr', self)
        self.button_triangle.setFixedWidth(70)
        self.button_comparasion = QtGui.QPushButton(u'сравн', self)
        self.button_comparasion.setFixedWidth(70)
        self.button_random = QtGui.QPushButton(u'заполнить', self)
        self.button_random.setFixedWidth(70)


        grid = QtGui.QGridLayout()
        grid.setSpacing(10)


        grid.addWidget(self.edit, 6, 0)
        grid.addWidget(self.edit2, 6, 1)
        grid.addWidget(self.edit3, 6, 5)
        grid.addWidget(self.edit4, 6, 6)

        grid.addWidget(self.button_plus, 0, 0)
        grid.addWidget(self.button_minus, 0, 1)
        grid.addWidget(self.button_mult, 0, 2)
        grid.addWidget(self.button_trans, 0, 3)
        grid.addWidget(self.button_triangle, 0, 4)
        grid.addWidget(self.button_random, 0, 5)

        grid.addWidget(self.button_comparasion, 0, 7)

        self.table = QtGui.QTableWidget(parent)
        self.matrix_2 = QtGui.QTableWidget(parent)

        self.table.setColumnCount(0)
        self.table.setRowCount(0)
        grid.addWidget(self.table, 1, 0, 5, 5)

        self.matrix_2.setColumnCount(0)
        self.matrix_2.setRowCount(0)
        grid.addWidget(self.matrix_2, 1, 5, 5, 10)



        self.decision = QtGui.QTableWidget(parent)
        self.decision.setColumnCount(0)
        self.decision.setRowCount(0)
        grid.addWidget(self.decision, 7, 0, 10, 10)

        #тут назначем действие к событиям
        self.connect(self.edit, QtCore.SIGNAL("textChanged()"), lambda : self.set_table_size())
        self.connect(self.edit2, QtCore.SIGNAL("textChanged()"), lambda: self.set_table_size())
        self.connect(self.edit3, QtCore.SIGNAL("textChanged()"), lambda: self.set_table2_size())
        self.connect(self.edit4, QtCore.SIGNAL("textChanged()"), lambda: self.set_table2_size())

        self.connect(self.button_plus, QtCore.SIGNAL("clicked()"), lambda: self.result_matrix(self.matrix_summ(self.select_matrix1(self.matrix1, self.table),
                                                                                                               self.select_matrix2(self.matrix2, self.matrix_2))))

        self.connect(self.button_minus, QtCore.SIGNAL("clicked()"), lambda : self.result_matrix(self.matrix_minus(self.select_matrix1(self.matrix1, self.table),
                                                                                                                  self.select_matrix2(self.matrix2, self.matrix_2))))

        self.connect(self.button_trans, QtCore.SIGNAL("clicked()"), lambda  : self.matrix_trans(self.select_matrix1(self.matrix1, self.table)))

        self.connect(self.button_mult, QtCore.SIGNAL("clicked()"), lambda : self.result_matrix(self.matrix_mult(self.select_matrix1(self.matrix1, self.table),
                                                                                               self.select_matrix2(self.matrix2, self.matrix_2))))

        self.connect(self.button_triangle, QtCore.SIGNAL("clicked()"), lambda : self.result_matrix(self.triangle_mattix(self.select_matrix1(self.matrix1, self.table))))

        self.connect(self.button_comparasion, QtCore.SIGNAL("clicked()"), lambda : self.comarasion(self.select_matrix1(self.matrix1, self.table),
                                                                                              self.select_matrix1(self.matrix2, self.matrix_2)))
        self.connect(self.button_random, QtCore.SIGNAL("clicked()"), lambda : self.random_matrix_generate())

        # self.connect(button_plus, QtCore.SIGNAL("clicked()"), lambda : select_matrix1(matrix1, table))
        # self.connect(button_minus, QtCore.SIGNAL("clicked()"), lambda : QtGui.QMessageBox.about(self, "ep", str(col_row)))


        self.table.setItem(2, 2, QtGui.QTableWidgetItem('12'))
        self.setLayout(grid)



        # table.setGeometry(120, 140, 130, 180)

        # print (table.getContentsMargins())


        self.col_row = 0;
        self.matrix1 = []
        self.matrix2 = []
        self.matrix_res = []
        self.tmp_matrix = []


    def set_table_size(self):
            # table = QtGui.QTableWidget(parent)

        if ((int(self.edit.toPlainText()) <= 10) and (int(self.edit2.toPlainText()) <= 10)):
            self.table.setColumnCount(int(self.edit.toPlainText()))
            self.table.setRowCount(int(self.edit2.toPlainText()))

            # self.matrix_2.setColumnCount(int(self.edit.toPlainText()))
            # self.matrix_2.setRowCount(int(self.edit2.toPlainText()))

            self.decision.setColumnCount(int(self.edit.toPlainText()))
            self.decision.setRowCount(int(self.edit2.toPlainText()))

            for i in range(int(self.edit.toPlainText())):
                self.table.horizontalHeader().resizeSection(i, 40)
                # self.matrix_2.horizontalHeader().resizeSection(i, 40)
                self.decision.horizontalHeader().resizeSection(i, 40)


        else:
             QtGui.QMessageBox.about(self, u"Warning", u'Данная программа может считать лишь матрицы 10х10, для расширения функционала необходимо поплнить баланс разработчика')

        # print a[1]
    def set_table2_size(self):
        self.matrix_2.setColumnCount(int(self.edit3.toPlainText()))
        self.matrix_2.setRowCount(int(self.edit4.toPlainText()))


        for i in range(int(self.edit3.toPlainText())):
            self.matrix_2.horizontalHeader().resizeSection(i, 40)


        #очищаем от мусора, подгружаем матрицу
    def select_matrix1(self, matrix, table):
        del matrix[:]
        try:
            for i in range(int(self.edit2.toPlainText())):
                matrix.append([])
                for j in range(int(self.edit.toPlainText())):
                    matrix[i].append(float(table.item(i, j).text()))
        except Exception:
            QtGui.QMessageBox.about(self, "erroe", u"Заполните цифрами все ячейки таблицы")
        return matrix

    def select_matrix2(self, matrix, table):
        del matrix[:]
        try:
            for i in range(int(self.edit3.toPlainText())):
                matrix.append([])
                for j in range(int(self.edit4.toPlainText())):
                    matrix[i].append(float(table.item(i, j).text()))
        except Exception:
            QtGui.QMessageBox.about(self, "erroe", u"Заполните цифрами все ячейки таблицы")
        return matrix




        # сложение
    def matrix_summ(self, m1, m2):
        if (int(self.edit.toPlainText()) == int(self.edit3.toPlainText()) and
            int(self.edit2.toPlainText()) == int(self.edit4.toPlainText())):
            del self.tmp_matrix[:]
            for i in range(int(self.edit2.toPlainText())):
                self.tmp_matrix.append([])
                for j in range(int(self.edit.toPlainText())):
                    self.tmp_matrix[i].append(int(m1[i][j]) + int(m2[i][j]))

            print self.tmp_matrix
            return self.tmp_matrix
        else:
            QtGui.QMessageBox.about(self, "error", u'Для сложения размер матриц должен быть идентичен')

    def matrix_minus(self, m1, m2):
        if (int(self.edit.toPlainText()) == int(self.edit3.toPlainText()) and
                    int(self.edit2.toPlainText()) == int(self.edit4.toPlainText())):
            del self.tmp_matrix[:]
            for i in range(int(self.edit2.toPlainText())):
                self.tmp_matrix.append([])
                for j in range(int(self.edit.toPlainText())):
                    self.tmp_matrix[i].append(int(m1[i][j]) - int(m2[i][j]))
            return self.tmp_matrix
        else:
            QtGui.QMessageBox.about(self, "error", u'Для вычитания размер матриц должен быть идентичен')

    def matrix_trans(self, m):
        del self.tmp_matrix[:]
        self.decision.setColumnCount(int(self.edit2.toPlainText()))
        self.decision.setRowCount(int(self.edit.toPlainText()))


        for i in range(int(self.edit.toPlainText())):
            self.tmp_matrix.append([])
            for j in range(int(self.edit2.toPlainText())):
                self.tmp_matrix[i].append(int(m[j][i]))

        for i in range(int(self.edit.toPlainText())):
            for j in range(int(self.edit2.toPlainText())):
                self.decision.setItem(i, j, QtGui.QTableWidgetItem(str(self.tmp_matrix[i][j])))
        return self.tmp_matrix

    def matrix_mult(self, m1, m2):
        if (int(self.edit2.toPlainText()) == int(self.edit3.toPlainText())):

            self.decision.setColumnCount(int(self.edit4.toPlainText()))
            self.decision.setRowCount(int(self.edit2.toPlainText()))

            del self.tmp_matrix[:]
            summ = 0
            for k in range (int(self.edit2.toPlainText())):
                self.tmp_matrix.append([])
                for i in range(int(self.edit3.toPlainText())):
                    for j in range(int(self.edit2.toPlainText())):

                        summ += m1[i][j] * m2[j][i]

                        self.tmp_matrix[k].append(summ)
                    summ = 0
            return self.tmp_matrix
        else:
            QtGui.QMessageBox.about(self, "error", u'Количество столбцов первой матрицы должно совпадать с количеством строк второй!')


        # вывод резульата любых операций в 3 столбец
    def result_matrix(self, m):
        print m
        for i in range(int(self.edit2.toPlainText())):
            for j in range(int(self.edit.toPlainText())):
                self.decision.setItem(i, j, QtGui.QTableWidgetItem(str(m[i][j])))


        #приведение к треугольному виду
    def triangle_mattix(self, m):
        if  (int(self.edit.toPlainText())) == int(self.edit2.toPlainText()):
            mat = np.array(m)
            mat[0] = (mat[0]) / mat[0][0]

            k = 0

            while k < mat.shape[1]:
                for i in range(k + 1, mat.shape[0]):
                    mat[i] = mat[i] / mat[i][k]
                    mat[i] = mat[i] - mat[k]
                    mat[i] = mat[i] / mat[i][k + 1]
                k += 1

            print mat
            return mat
        else:
            QtGui.QMessageBox.about(self, "error", u'матрица должна быть квадратной')

        #сравнение
    def comarasion(self, m1, m2):
        if (int(self.edit.toPlainText()) == int(self.edit3.toPlainText()) and
                    int(self.edit2.toPlainText()) == int(self.edit4.toPlainText())):
            comp = False
            for i in range(int(self.edit2.toPlainText())):
                for j in range(int(self.edit.toPlainText())):
                    if  m1[i][j] != m2[i][j]:
                        comp = True
            if comp == False:
                QtGui.QMessageBox.about(self, u'сравнение', u'матрицы идентичны')
            else:
                QtGui.QMessageBox.about(self, u'сравнение', u'матрицы не идентичны')
        else:
            QtGui.QMessageBox.about(self, u'сравнение', u'матрицы не идентины')

    def random_matrix_generate(self):
        for i in range(int(self.edit2.toPlainText())):
            for j in range(int(self.edit.toPlainText())):
                self.table.setItem(i, j, QtGui.QTableWidgetItem(str(random.random() * 100)))

        for i in range(int(self.edit4.toPlainText())):
            for j in range(int(self.edit3.toPlainText())):
                self.matrix_2.setItem(i, j, QtGui.QTableWidgetItem(str(random.random() * 100)))




# 11
app = QtGui.QApplication(sys.argv)
rn = AppWind()
rn.show()
sys.exit(app.exec_())









